// +build linux

package glue

import (
	"fmt"
	"net"
	"strings"

	"github.com/godbus/dbus"
)

var dbusname = "org.bitbucket.jatone.glue"
var dbuspath = "/org/bitbucket/jatone/glue"

func new() (Bluetooth, error) {
	conn, err := dbus.SystemBus()
	return bluez{
		conn:       conn,
		obj:        conn.Object("org.bluez", "/org/bluez"),
		objManager: conn.Object("org.bluez", "/"),
	}, err
}

type bluez struct {
	conn       *dbus.Conn
	obj        dbus.BusObject
	objManager dbus.BusObject
}

func (t bluez) GetManagedObjects() (map[dbus.ObjectPath]map[string]map[string]dbus.Variant, error) {
	objects := map[dbus.ObjectPath]map[string]map[string]dbus.Variant{}
	err := t.objManager.Call("org.freedesktop.DBus.ObjectManager.GetManagedObjects", 0).Store(&objects)
	return objects, err
}

func (t bluez) ListDevices(filters ...DeviceFilter) ([]Device, error) {
	devices := []Device{}
	objects, err := t.GetManagedObjects()
	if err != nil {
		return []Device{}, err
	}

	for path, interfaces := range objects {
		var dev Device
		devImpl := device{
			path: path,
			conn: t.conn,
			obj:  t.conn.Object("org.bluez", path),
		}

		properties, ok := interfaces["org.bluez.Device1"]
		if !ok {
			continue
		}

		for k, v := range properties {
			switch k {
			case "Name":
				dev.Name = v.Value().(string)
			case "Paired":
				dev.Paired = v.Value().(bool)
			case "Alias":
				dev.Alias = v.Value().(string)
			case "Address":
				dev.Address = v.Value().(string)
			case "Trusted":
				dev.Trusted = v.Value().(bool)
			case "UUIDs":
				dev.UUIDs = v.Value().([]string)
			case "Blocked":
				dev.Blocked = v.Value().(bool)
			case "Connected":
				dev.Connected = v.Value().(bool)
			case "LegacyPairing":
				dev.LegacyPairing = v.Value().(bool)
			case "Class":
				dev.Class = v.Value().(uint32)
			case "Icon":
				dev.Icon = v.Value().(string)
			case "Adapter":
				devImpl.Adapter = v.Value().(dbus.ObjectPath)
			case "Modalias":
				dev.Modalias = v.Value().(string)
			case "RSSI":
				dev.RSSI = v.Value().(int16)
			default:
				fmt.Printf("unknown property %s -> %#v\n", k, v)
			}
		}

		dev.implementation = devImpl

		if matchDevice(dev, filters...) {
			devices = append(devices, dev)
		}
	}

	if len(devices) == 0 {
		err = ErrDeviceNotFound
	}
	return devices, err
}

func (t bluez) FindDevice(filters ...DeviceFilter) (Device, error) {
	devices, err := t.ListDevices(filters...)
	if err != nil {
		return Device{}, err
	}

	return devices[0], nil
}

func (t bluez) ListAdapters() ([]Adapter, error) {
	adapters := make([]Adapter, 0, 10)
	objects, err := t.GetManagedObjects()
	if err != nil {
		return adapters, err
	}

	for path, interfaces := range objects {
		var adapt Adapter
		adapterImpl := adapter{
			path: path,
			obj:  t.conn.Object("org.bluez", path),
		}

		properties, ok := interfaces["org.bluez.Adapter1"]
		if !ok {
			continue
		}

		for k, v := range properties {
			switch k {
			case "Name":
				adapt.Name = v.Value().(string)
			case "Alias":
				adapt.Alias = v.Value().(string)
			case "Class":
				adapt.Class = v.Value().(uint32)
			case "Discoverable":
				adapt.Discoverable = v.Value().(bool)
			case "Pairable":
				adapt.Pairable = v.Value().(bool)
			case "Address":
				adapt.Address = v.Value().(string)
			case "Powered":
				adapt.Powered = v.Value().(bool)
			case "DiscoverableTimeout":
				adapt.DiscoverableTimeout = v.Value().(uint32)
			case "PairableTimeout":
				adapt.PairableTimeout = v.Value().(uint32)
			case "Discovering":
				adapt.Discovering = v.Value().(bool)
			case "Modalias":
				adapt.Modalias = v.Value().(string)
			case "UUIDs":
				adapt.UUIDs = v.Value().([]string)
			default:
				fmt.Printf("unknown property %s -> %#v\n", k, v.Value())
			}
		}

		adapt.implementation = adapterImpl

		adapters = append(adapters, adapt)
	}

	return adapters, nil
}

func (t bluez) Listen(p Profile) (net.Listener, error) {
	// channel cap of 1 to allow inline device.ConnectProfile(); listener.Accept()
	l := listener{profile: p, profileManager: t, fdchannel: make(chan int, 1)}
	err := t.conn.Export(l, profileToObjectPath(p), "org.bluez.Profile1")

	return l, err
}

func (t bluez) Announce(p Profile) error {
	dbusname := profileToDBUSName(p)
	dbuspath := profileToObjectPath(p)

	reply, err := t.conn.RequestName(dbusname, dbus.NameFlagDoNotQueue)
	if err != nil {
		return err
	}

	if reply != dbus.RequestNameReplyPrimaryOwner {
		return fmt.Errorf(dbusname, "already taken")
	}

	options := map[string]dbus.Variant{
		"Name":                  dbus.MakeVariant(p.Name),
		"Service":               dbus.MakeVariant(p.Service),
		"Role":                  dbus.MakeVariant(p.Role),
		"RequireAuthentication": dbus.MakeVariant(p.RequireAuthentication),
		"RequireAuthorization":  dbus.MakeVariant(p.RequireAuthorization),
		"AutoConnect":           dbus.MakeVariant(p.AutoConnect),
	}

	if p.Channel >= 0 {
		options["Channel"] = dbus.MakeVariant(uint16(p.Channel))
	}

	if p.PSM >= 0 {
		options["PSM"] = dbus.MakeVariant(uint16(p.PSM))
	}

	return t.obj.Call("org.bluez.ProfileManager1.RegisterProfile", 0,
		dbuspath,
		p.UUID,
		options,
	).Store()
}

func (t bluez) Suppress(p Profile) error {
	return t.obj.Call("org.bluez.ProfileManager1.UnregisterProfile", 0,
		profileToObjectPath(p),
	).Store()
}

func profileToObjectPath(p Profile) dbus.ObjectPath {
	return dbus.ObjectPath(strings.Join([]string{dbuspath, p.Name}, "/"))
}

func profileToDBUSName(p Profile) string {
	return strings.Join([]string{dbusname, p.Name}, ".")
}

func matchDevice(d Device, filters ...DeviceFilter) bool {
	for _, filter := range filters {
		if !filter.Match(d) {
			return false
		}
	}

	return true
}
