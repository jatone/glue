package glue

// DeviceImpl - bluetooth library specific code.
type DeviceImpl interface {
	Connect(Device) error
	Disconnect(Device) error
	ConnectProfile(d Device, UUID string) error
	DisconnectProfile(d Device, UUID string) error
	Pair(Device) error
	CancelPairing(Device) error
}

// DeviceFilter - device filters for devices.
type DeviceFilter interface {
	Match(Device) bool
}

// DeviceFilterFunc - pure function device filter
type DeviceFilterFunc func(Device) bool

// Match - See DeviceFilter
func (t DeviceFilterFunc) Match(d Device) bool {
	return t(d)
}

// Device - device details and interface
type Device struct {
	Name           string
	Alias          string
	Address        string
	Modalias       string
	Class          uint32
	Paired         bool
	Connected      bool
	Blocked        bool
	Trusted        bool
	LegacyPairing  bool
	Icon           string
	UUIDs          []string
	RSSI           int16
	implementation DeviceImpl
}

func (t Device) Connect() error {
	return t.implementation.Connect(t)
}

func (t Device) Disconnect() error {
	return t.implementation.Disconnect(t)
}

func (t Device) ConnectProfile(UUID string) error {
	return t.implementation.ConnectProfile(t, UUID)
}

func (t Device) DisconnectProfile(UUID string) error {
	return t.implementation.DisconnectProfile(t, UUID)
}

func (t Device) Pair() error {
	return t.implementation.Pair(t)
}

func (t Device) CancelPairing() error {
	return t.implementation.CancelPairing(t)
}

var DeviceFilters = struct {
	Name      func(string) DeviceFilter
	Alias     func(string) DeviceFilter
	Address   func(string) DeviceFilter
	UUID      func(string) DeviceFilter
	Paired    func(bool) DeviceFilter
	Connected func(bool) DeviceFilter
}{
	Name:      nameFilter,
	Alias:     aliasFilter,
	Address:   addressFilter,
	UUID:      uuidFilter,
	Paired:    pairedFilter,
	Connected: connectedFilter,
}

func nameFilter(name string) DeviceFilter {
	return DeviceFilterFunc(func(d Device) bool {
		return d.Name == name
	})
}

func aliasFilter(alias string) DeviceFilter {
	return DeviceFilterFunc(func(d Device) bool {
		return d.Alias == alias
	})
}

func addressFilter(address string) DeviceFilter {
	return DeviceFilterFunc(func(d Device) bool {
		return d.Address == address
	})
}

func uuidFilter(UUID string) DeviceFilter {
	return DeviceFilterFunc(func(d Device) bool {
		for _, uuid := range d.UUIDs {
			if uuid == UUID {
				return true
			}
		}

		return false
	})
}

func pairedFilter(paired bool) DeviceFilter {
	return DeviceFilterFunc(func(d Device) bool {
		return d.Paired == paired
	})
}

func connectedFilter(connected bool) DeviceFilter {
	return DeviceFilterFunc(func(d Device) bool {
		return d.Connected == connected
	})
}
