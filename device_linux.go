// +build linux

package glue

import "github.com/godbus/dbus"

type device struct {
	conn    *dbus.Conn
	obj     dbus.BusObject
	path    dbus.ObjectPath
	Adapter dbus.ObjectPath
}

func (t device) Connect(_ Device) error {
	return t.obj.Call("org.bluez.Device1.Connect", 0).Store()
}

func (t device) Disconnect(_ Device) error {
	return t.obj.Call("org.bluez.Device1.Disconnect", 0).Store()
}

func (t device) ConnectProfile(_ Device, UUID string) error {
	return t.obj.Call("org.bluez.Device1.ConnectProfile", 0, UUID).Store()
}

func (t device) DisconnectProfile(_ Device, UUID string) error {
	return t.obj.Call("org.bluez.Device1.DisconnectProfile", 0, UUID).Store()
}

func (t device) Pair(_ Device) error {
	return t.obj.Call("org.bluez.Device1.Pair", 0).Store()
}

func (t device) CancelPairing(_ Device) error {
	return t.obj.Call("org.bluez.Device1.CancelPairing", 0).Store()
}
