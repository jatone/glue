package glue

// Profile describes a bluetooth profile
// Name - name of the profile
// UUID - uuid identifying the profile.
// Service - alternate UUID describing the profile if client/server need a custom
// UUID.
// Role - RFCOMM profile role (server/client)
// Channel - RFCOMM port 0-30, -1 -> disable rfcomm, 0 -> random port, 1-30 specific port.
// PSM - L2CAP port, -1 -> disable l2cap, 0 -> random port.
type Profile struct {
	UUID                  string
	Name                  string
	Service               string
	Role                  string
	Channel               int
	PSM                   int
	RequireAuthentication bool
	RequireAuthorization  bool
	AutoConnect           bool
}
