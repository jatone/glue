package glue

import "net"

// New - returns a new reference to the bluetooth api.
func New() (Bluetooth, error) {
	return new()
}

// Bluetooth grants access to many of the interfaces required for interacting
// with bluetooth devices.
// ServiceDiscoveryProtocol - interface for interacting with the SDP server.
// DeviceDiscovery - interface for discovering devices.
// Network interface for creating network connections.
type Bluetooth interface {
	ServiceDiscoveryProtocol
	DeviceDiscovery
	Network
}

// Network interface for creating network connections.
// Listen - takes the provided profile and uses it to generate an connection
// works for both clients and servers. clients will return connections when
// they detect a device with the matching service. servers will return connections
// when a client attempts to connect to them.
type Network interface {
	Listen(p Profile) (net.Listener, error)
}

// DeviceDiscovery interface for discovering devices.
// ListDevices - lists all the devices that match the provided filters.
// if none match an error is returned.
// FindDevice - returns the first device that matches all the provided filters.
// if none match an error is returned.
type DeviceDiscovery interface {
	ListDevices(...DeviceFilter) ([]Device, error)
	FindDevice(...DeviceFilter) (Device, error)
}

// ServiceDiscoveryProtocol (SDP) interface to the SDP server.
type ServiceDiscoveryProtocol interface {
	// Announce - register a service with SDP.
	Announce(Profile) error
	// Suppress - remove a service with SDP.
	Suppress(Profile) error
}
