## Glue - warning extremely rough library (i.e. beta)

### bluetooth binding library for linux (currently, will take pull requests for other platforms)
* integrates with bluez.
* supports registering services with SDP.
* supports accepting connections using RFCOMM/l2cap.
* net package Listener style connections.
* currently requires the bluez daemon to be started in compatability mode (--compat)

### gotchas ###
* currently hardcoding sockets back to blocking mode from non-blocking mode
  apparently bluez decided that they shouldn't allow you to specified the socket
  options via the dbus api and instead use non-defaults and expect you to change
  them once they hand you the socket using fcntl and syscall.Setsockopt.
  if anyone ends up using this library I'm very open to changing this behavior
  via a pull request.
* bluez behaves oddly with respect to the role option on profiles, i.e) l2cap
  flat out doesn't work if a role is specified. not sure if they is according to
  spec or not. so if you want to listen to both l2cap and rfcomm channels, don't
  specify a role in your profile.
* since bluez is pushing the dbus api in order to correctly use glue you need to
  configure dbus properly, and example configuration can be found at
  resources/bluez-dbus.conf. see setup for further details about what to do with
  this file.

### setup ###
```
# sadly bluez is going forward is pushing the dbus api as the standard.
go get github.com/godbus/dbus
go get bitbucket.org/jatone/glue
cp src/bitbucket.org/jatone/glue/resources /etc/dbus-1/system.d/org.bitbucket.jatone.glue.example.conf
```
#### development dependencies.
* ginkgo/gomega (testing framework)

### contribution guidelines ###
* include tests (kind of silly since currently I haven't written any but this started as a subproject to another project and tests will be written once I finish the base work)
