package main

import (
	"flag"
	"log"
	"net"
	"time"

	"bitbucket.org/jatone/glue"
)

var address = flag.String("address", "C0:33:5E:E1:CD:52", "device to connect to")
var profileUUID = flag.String("profile", "00005007-0000-1000-8000-0002ee000001", "profile to connect to")

func main() {
	flag.Parse()

	profile := glue.Profile{
		Name: "dieclient",
		UUID: *profileUUID,
		Role: "client",
	}

	btlistener, err := glue.Listen(profile)
	stop("listen failed", err)
	defer btlistener.Close()
	defer glue.Suppress(profile)

	device, err := glue.FindDevice(
		glue.DeviceFilters.Address(*address),
		glue.DeviceFilters.UUID(*profileUUID),
		glue.DeviceFilters.Paired(true),
		glue.DeviceFilters.Connected(true),
	)

	stop("Device lookup failed", err)
	stop("ConnectProfile failed", device.ConnectProfile(profile.UUID))
	log.Println("Accepting Connections")
	for {
		var c net.Conn
		var err error
		if c, err = btlistener.Accept(); err != nil {
			log.Println("Error Accepting Connection", err)
			break
		}
		go func() {
			log.Println("Connect Accepted")
			time.Sleep(10 * time.Second)
			log.Println("closing connection")
			c.Close()
		}()
	}
}

func acceptLoop(l net.Listener, out chan net.Conn) {
	for {
		var c net.Conn
		var err error
		log.Println("Listening for connection")
		if c, err = l.Accept(); err != nil {
			log.Println("Error Accepting Connection", err)
			break
		}
		out <- c
	}
}

func stop(message ...interface{}) {
	e := message[len(message)-1]
	if e != nil {
		log.Fatalln(message...)
	}
}
