package main

import (
	"flag"
	"fmt"
	"log"
	"net"

	"bitbucket.org/jatone/glue"
)

var address = flag.String("address", "C0:33:5E:E1:CD:52", "device to connect to")
var profileUUID = flag.String("profile", "00005007-0000-1000-8000-0002ee000001", "profile to connect to")

func main() {
	connections := make(chan net.Conn)

	profile := glue.Profile{
		Name: "example",
		UUID: *profileUUID,
	}

	l, err := glue.Listen(profile)
	stop("listen failed", err)
	defer l.Close()
	defer glue.Suppress(profile)

	go acceptLoop(l, connections)

	for {
		select {
		case c := <-connections:
			log.Println("Reading from new connection")
			go func() {
				readLoop(c)
			}()
		}
	}
}

func stop(message ...interface{}) {
	e := message[len(message)-1]
	if e != nil {
		log.Fatalln(message...)
	}
}

func acceptLoop(l net.Listener, out chan net.Conn) {
	for {
		var c net.Conn
		var err error
		log.Println("Listening for connection")
		if c, err = l.Accept(); err != nil {
			log.Println("Error Accepting Connection", err)
			continue
		}
		out <- c
	}
}

func readLoop(in net.Conn) {
	defer in.Close()
	buff := make([]byte, 0, 10)
	for {
		n, err := in.Read(buff)
		if err != nil {
			fmt.Println("error reading", err)
			return
		}

		fmt.Println("read", n, "bytes")
	}
}
