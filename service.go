package glue

type Session interface {
	Disconnect() error
}

type Service interface {
	Info() Profile
}

type service struct {
	Profile
}

func (t service) Info() Profile {
	return t.Profile
}

func NewService(d Profile) Service {
	return service{d}
}
