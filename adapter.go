package glue

// Adapter Experimental not implemented fully.
type Adapter struct {
	Name                string
	Alias               string
	Modalias            string
	Pairable            bool
	Discoverable        bool
	Powered             bool
	Discovering         bool
	Address             string
	Class               uint32
	PairableTimeout     uint32
	DiscoverableTimeout uint32
	UUIDs               []string
	implementation      AdapterImpl
}

// AdapterImpl - bluetooth library specific code.
// i.e. implementation for bluez, osx, etc.
type AdapterImpl interface {
	StartDiscovery(Adapter) error
	StopDiscovery(Adapter) error
	RemoveDevice(Adapter, Device) error
}
