package glue

import (
	"fmt"
	"log"
	"net"
)

// utility functions, global state, and constants for the glue library.

// DisableProtocol used to disable a protocol on a profile
// Profile{Channel: glue.DisableProtocol}
const DisableProtocol = -1

// ErrDeviceNotFound - returned when no devices match the provided filters
var ErrDeviceNotFound = fmt.Errorf("device not found")

// ErrClosedSocket - returned from Accept if the socket is shutdown.
var ErrClosedSocket = fmt.Errorf("socket closed")

// internal reference to the bluetooth connection.
var bluetooth = Bluetooth(must(new()))

func must(b Bluetooth, err error) Bluetooth {
	if err != nil {
		log.Println(err)
	}

	return b
}

// Default - provides access to the default bluetooth
// reference.
func Default() Bluetooth {
	return bluetooth
}

// Listen - See the Network interface.
func Listen(p Profile) (net.Listener, error) {
	return bluetooth.Listen(p)
}

// ListDevices - See the DeviceDiscovery interface.
func ListDevices(filters ...DeviceFilter) ([]Device, error) {
	return bluetooth.ListDevices(filters...)
}

// FindDevice - See the DeviceDiscovery interface.
func FindDevice(filters ...DeviceFilter) (Device, error) {
	return bluetooth.FindDevice(filters...)
}

// Announce - See the ServiceDiscoveryProtocol interface.
func Announce(p Profile) error {
	return bluetooth.Announce(p)
}

// Suppress - See the ServiceDiscoveryProtocol interface.
func Suppress(p Profile) error {
	return bluetooth.Suppress(p)
}
