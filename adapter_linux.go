// +build linux

package glue

import (
	"fmt"

	"github.com/godbus/dbus"
)

type adapter struct {
	path dbus.ObjectPath
	obj  dbus.BusObject
}

func (t adapter) StartDiscovery(Adapter) error {
	return fmt.Errorf("not implemented")
}

func (t adapter) StopDiscovery(Adapter) error {
	return fmt.Errorf("not implemented")
}

func (t adapter) RemoveDevice(Adapter, Device) error {
	return fmt.Errorf("not implemented")
}
