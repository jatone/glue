// +build linux

package glue

// /*
// #cgo LDFLAGS: -lbluetooth
// #include <bluetooth/bluetooth.h>
// #include <bluetooth/rfcomm.h>
// */
// import "C"
import (
	"fmt"
	"log"
	"net"
	"syscall"
	"time"

	"github.com/godbus/dbus"
)

// bluezConn implements the net.Conn interface for the bluez library.
type bluezConn struct {
	fd int
}

func (d bluezConn) Read(b []byte) (int, error) {
	n, err := syscall.Read(d.fd, b)
	if err != nil {
		n = 0
	}
	return n, err
}

func (d bluezConn) Write(b []byte) (int, error) {
	n, err := syscall.Write(d.fd, b)
	if err != nil {
		n = 0
	}
	return n, err
}

func (d bluezConn) Close() error {
	return syscall.Close(d.fd)
}

func (d bluezConn) LocalAddr() net.Addr {
	return nil
}

func (d bluezConn) RemoteAddr() net.Addr {
	return nil
}

func (d bluezConn) SetDeadline(t time.Time) error {
	return fmt.Errorf("Not Implemented")
}

func (d bluezConn) SetReadDeadline(t time.Time) error {
	return fmt.Errorf("Not Implemented")
}

func (d bluezConn) SetWriteDeadline(t time.Time) error {
	return fmt.Errorf("Not Implemented")
}

type listener struct {
	profile        Profile
	profileManager Bluetooth
	fdchannel      chan int
}

func (t listener) Accept() (net.Conn, error) {
	var fd int
	var ok bool

	if fd, ok = <-t.fdchannel; ok {
		return bluezConn{fd: fd}, nil
	}

	return nil, ErrClosedSocket
}

func (t listener) Close() error {
	defer close(t.fdchannel)
	return t.profileManager.Suppress(t.profile)
}

func (t listener) Addr() net.Addr {
	return nil
}

func (t listener) Release() *dbus.Error {
	log.Println("Release Invoked")
	close(t.fdchannel)
	return nil
}

func (t listener) NewConnection(obj dbus.ObjectPath, fd dbus.UnixFD, dict map[string]dbus.Variant) *dbus.Error {
	log.Printf("New Connection Recieved %s, %d, %v\n", obj, int(fd), dict)
	// major hack due to limitations and defaults from the bluez library.
	// set the socket back to blocking mode.
	a1, _, err := syscall.Syscall(syscall.SYS_FCNTL, uintptr(fd), syscall.F_GETFL, 0)
	if err != 0 {
		return &dbus.Error{Name: err.Error()}
	}
	syscall.Syscall(syscall.SYS_FCNTL, uintptr(fd), syscall.F_SETFL, a1&^syscall.O_NONBLOCK)
	val, _ := syscall.GetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_RCVLOWAT)
	log.Println("socket opt SO_RCVLOWAT", val)
	val, _ = syscall.GetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_RCVBUF)
	log.Println("socket opt SO_RCVBUF", val)
	val, _ = syscall.GetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_SNDLOWAT)
	log.Println("socket opt SO_SNDLOWAT", val)
	val, _ = syscall.GetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_RCVLOWAT)
	log.Println("socket opt SO_RCVLOWAT", val)
	// end major hack.

	t.fdchannel <- int(fd)
	return nil
}

func (t listener) RequestDisconnection(device dbus.ObjectPath) *dbus.Error {
	log.Println("Request Disconnection", device)
	return nil
}
